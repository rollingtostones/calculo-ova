---
layout: post
title:  "Bibliografía"
date:   2020-06-09 18:51:43 -0400
categories: bibliografia
tag: 1
---

# Bibliografía

En esta sección podras acceder a la bibliografía de la materia y descargar material de lectura.

<div class="grid">
    <div class="book">
        <a href="https://drive.google.com/file/d/1OGiESsIWKknd_j6WuSzA1ce33dHaen1E/view?usp=sharing">
            <img src="/assets/images/coverbookfinks.jpg" alt="bookCover1" class="cover">
            <div class="bookInfo">
                <bold style="font-weight: bold; color: black; display: block">
                    Métodos númericos con MATLAB
                </bold>
                <span style="font-size: 0.9em; opacity: 50%; color: black;">
                    John H. Mathews, Kurtis D. Fink
                </span>
            </div>
        </a>
    </div>

    <div class="book">
        <a href="https://drive.google.com/file/d/17hD1yp6ynaq7eM8bxY5OHvDGtNaLs3h6/view?usp=sharing">
            <img src="/assets/images/coverbookburden.jpg" alt="bookCover2" class="cover">
            <div class="bookInfo">
                <bold style="font-weight: bold; color: black; display: block">
                    Análisis numérico
                </bold>
                <span style="font-size: 0.9em; opacity: 50%; color: black;">
                    Richard L. Burden, J. Douglas Faires
                </span>
            </div>
        </a>
    </div>

    <div class="book">
        <a href="https://drive.google.com/file/d/1kRDnSXXV_nN8bRDVShO0m-07ckaQpuuL/view?usp=sharing">
            <img src="/assets/images/coverbookchapra.jpg" alt="bookCover3" class="cover">
            <div class="bookInfo">
                <bold style="font-weight: bold; color: black; display: block">
                    Métodos numéricos para ingenieros
                </bold>
                <span style="font-size: 0.9em; opacity: 50%; color: black;">
                    Steven C. Chapra, Raymond R.Canale
                </span>
            </div>
        </a>
    </div>

</div>
