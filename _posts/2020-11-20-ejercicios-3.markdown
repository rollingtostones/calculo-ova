---
layout: post
title:  "Ejemplos y ejercicios propuestos"
date:   2020-11-20 18:51:43 -0400
categories: senluv
tag: 11
---

# Ejemplos
<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Y-43pTrEgMo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

# Ejercicios Propuestos

1- Determine las raíces de {% katex %} f(x)= -0.5x^2+2.5x+4.5 {% endkatex %} usando el método de bisección con 3 iteraciones, con valores iniciales {% katex %} x_l=5,  x_\mu=10 {% endkatex %}

2- Determine las raíces de {% katex %} f(x)= 2x^3-11.7x^2+17.7x-5 {% endkatex %} con el método de Newtón-Raphson con 3 iteraciones, {% katex %} x_0=3,  \delta=0.001 {% endkatex %}

3- Determine las raíces de ñas ecuaciones no lineales simultaneas por medio de los métodos de Bisección y Newtón-Raphson, con valores iniciales {% katex %} Y=y=1.2 {% endkatex %}:

{% katex display%} Y=-x^2+x*0.75 {% endkatex %}

{% katex display%} Y+5xy=x^2 {% endkatex %}