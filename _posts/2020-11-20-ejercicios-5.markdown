---
layout: post
title:  "Ejemplos y ejercicios propuestos"
date:   2020-11-20 18:51:43 -0400
categories: in
tag: 11
---

# Ejemplos

<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/RL7KiVM0Fz4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

# Ejercicios Propuestos

1- Use la regla del trapecio compuesto con n=5
para aproximar la integral {% katex %} \int_{\frac{1}{2}} ^1  arcose(x)d_x {% endkatex %}

2- Emplee las reglas del trapecio y simpson {% katex %} \frac{1}{3} {% endkatex %}
para la función {% katex %} \int_0 ^3 x^2 e^x d_x {% endkatex %}

3- Aplique la regla de simpson {% katex %} \frac{3}{8} {% endkatex %} para {% katex %} \int_{0.5} ^{1.5} 14^{2x} d_x {% endkatex %}