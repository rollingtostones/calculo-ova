---
layout: post
title:  "Ejercicios propuestos"
date:   2020-11-20 18:51:43 -0400
categories: rpf
tag: 11
---

# Ejercicios Propuestos

1- Dados los siguientes números; realice la representación entera:

 - 222,2
 - -0,0033
 - -44,44

2- Determine el dígito más y menos significativo:

 - 222,333
 - 440,000
 - 0,0555

3- Encuentre la representación entera con 32 bits:

 - A=-907
 - A=907

