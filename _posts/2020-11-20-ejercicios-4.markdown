---
layout: post
title:  "Ejemplos y ejercicios propuestos"
date:   2020-11-20 18:51:43 -0400
categories: interpolacion
tag: 11
---

# Ejemplos

<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/c1IW5TgbxWo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

# Ejercicios Propuestos

1- Estime {% katex %} Ln 2 {% endkatex %} con un polinomio de interpolación de Newtón de tercer grado,
con los datos {% katex %} x_0=1, x_1=4, x_2=6, x_3=5 {% endkatex %}

2- Con los datos {% katex %} x_0=6, x_1=5, x_2=2 {% endkatex %} evalue
el polinomio {% katex %} Ln 2 {% endkatex %} de interpolación de Lagrange

3- Determine el polinomio de Lagrange que contiene los datos {% katex %}(1,-1), (-3,1), (5,2), (7,-3) {% endkatex %} . 